# Example queries using Postman

Here are some queries you can use with this project using Postman.

## Setting headers

The API is set up to expect JSON in the body. In Postman, switch to the "Headers" tab. Add one header called "Content-Type". Its value should be "application/json".

## Registering and creating a new user

There are only two endpoints that can be used without authentication: registering a new user, or logging in as an existing user. Here's an example request to register a new user (a PUT request to http://localhost/user/new):

```
{
	"user": {
		"username": "mike",
		"name": "Mike",
		"emailAddress": "mike@kocachic.com",
		"password": "secret1234"
	}
}
```

And here's a request to log in (a POST request to http://localhost/user/login):

```
{
	"user": {
		"username": "mike",
		"password": "secret1234"
	}
}
```

## CRUD API calls for templates

### Authentication

The /user/login endpoint will return to you an authentication token, which is a very long string. All other API calls require that authentication token to be present.

In Postman, go to the "Authentication" tab. Under "TYPE", select "Bearer Token". Paste in the token string that was returned to you when logging in.

Make sure to do this for *every* API call that requires user authentication.

### Inserting something new into the database

Use a PUT request to http://localhost:8000/template/new with the following JSON code in the body:

```
{
	"template": {
		"name": "another template",
		"scenes": [
			{
				"name": "scene 1",
				"resources": [
					{
						"resourceType": 0,
						"text": "this is some text"
					},
					{
						"resourceType": 1,
						"url": "s3://whatever/i.jpg"
					}
				]
			}
		]
	}
}
```

### Viewing the database

Use a GET request to http://localhost:8000/template/all

### Updating an existing template

Use a PATCH request to http://localhost:8000/template/update:

Note, when doing an update, that only the properties that are given by the client will be updated. If there are any properties not included in the client's request, they will remain unchanged in the database.

```
{
	"template": {
		"_id": "5af646fd8fa95317bf131fe8",
		"name": "today's new template",
		"scenes": [
			{
				"name": "scene 1",
				"resources": [
					{
						"resourceType": 0,
						"text": "this is some text"
					}
				]
			},
			{
				"name": "scene 2",
				"resources": [
					{
						"resourceType": 2,
						"url": "http://wherever"
					}
				]
			}
		]
	}
}
```

### Deleting an existing template in the database

Use a DELETE request to http://localhost:8000/template/delete:

```
{
	"templateID": "5af646fd8fa95317bf131fe8"
}
```

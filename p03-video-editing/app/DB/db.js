const mongoose = require('mongoose');

module.exports = {
	connect: (url) => {
		mongoose.Promise = global.Promise;
		mongoose.connect(url);
		var conn = mongoose.connection;
		conn.on('error', console.error.bind(console, 'MongoDB connection error'));
		return conn;
	}
};

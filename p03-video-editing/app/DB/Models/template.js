const mongoose = require('mongoose');
const scene = require('./scene');

const templateSchema = new mongoose.Schema({
	name: String,
	scenes: [scene.schema],	// mongoose subdocument
	owner: mongoose.Schema.Types.ObjectId,
	deleted: Boolean	// initially false
});

module.exports = {
	schema: templateSchema,
	model: mongoose.model('Template', templateSchema)
};

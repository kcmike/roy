const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
	username: { type: String, unique: true },
	name: String,
	emailAddress: String,
	passwordHash: String,
	languagePreference: String,	// use a combination of ISO 639 and ISO 3166 (e.g., "en-US")
	facebookCredentials: String,
	twitterCredentials: String,
	kakaoCredentials: String
});

module.exports = {
	schema: UserSchema,
	model: mongoose.model('User', UserSchema)
};

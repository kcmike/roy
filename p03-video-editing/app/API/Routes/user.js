const user = require('../../DB/Models/user');
const bcrypt = require('bcrypt-nodejs');

module.exports = {
	create: (accountInfo, callback) => {
		bcrypt.hash(accountInfo.password, null, null, (err, hashedPassword) => {
			if (err)	return callback(err);
			delete accountInfo.password;
			accountInfo.passwordHash = hashedPassword;
			user.model.create(accountInfo, callback);
			// TODO: send verification code
		});
	},
	login: (loginInfo, callback) => {
		user.model.findOne({ username: loginInfo.username }, (err, dbUser) => {
			if (err)	return callback(err);
			bcrypt.compare(loginInfo.password, dbUser.passwordHash, (err, passwordCorrect) => {
				if (err)	return callback(err);
				if (!passwordCorrect)	return callback(new Error('Username or password incorrect'));
				return callback(err, dbUser);
			});
		});
	}
}

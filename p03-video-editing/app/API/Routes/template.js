const template = require('../../DB/Models/template.js');
const scene = require('../../DB/Models/scene.js');

module.exports = {
	create: (user, newTemplate, callback) => {
		newTemplate.owner = user._id;
		newTemplate.deleted = false;
		template.model.create(newTemplate, callback);
	},
	queryAll: (user, callback) => {
		template.model.find({ owner: user._id, deleted: false }, callback);
	},
	update: (user, newTemplate, callback) => {
		newTemplate.owner = user._id;
		newTemplate.deleted = false;
		template.model.findOneAndUpdate({ _id: newTemplate._id, owner: user._id }, newTemplate, callback);
	},
	delete: (user, templateId, callback) => {
		newTemplate.deleted = true;
		template.model.findOneAndUpdate({ _id: templateId, owner: user._id }, newTemplate, callback);
	}
};

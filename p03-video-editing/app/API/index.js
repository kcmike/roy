const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const checkToken = require('express-jwt');
const template = require('./Routes/template');
const user = require('./Routes/user');
const config = require('../config');

var app = express();

function allRoutes() {
	app.use(bodyParser.json());	// all requests will have JSON-encoded data
	app.use(checkToken({ secret: config.security.jwtSecret }).unless({path: ['/user/new', '/user/login']}));	// req.user will be set
	app.put('/template/new', (req, res) => {
		template.create(req.user, req.body.template, (err) => {
			if (err)	handleError(err);
			return res.status(200).json({ status: 'OK' });
		});
	});
	app.get('/template/all', (req, res) => {
		template.queryAll(req.user, (err, templates) => {
			if (err)	handleError(err);
			return res.status(200).json({ status: 'OK', templates: templates });
		});
	});
	app.patch('/template/update', (req, res) => {
		template.update(req.user, req.body.template, (err) => {
			if (err)	handleError(err);
			return res.status(200).json({ status: 'OK' });
		});
	});
	app.delete('/template/delete', (req, res) => {
		template.delete(req.user, req.body.templateID, (err) => {
			if (err)	handleError(err);
			return res.status(200).json({ status: 'OK' });
		});
	});
	app.put('/user/new', (req, res) => {
		user.create(req.body.user, (err) => {
			if (err)	handleError(err);
			return res.status(200).json({ status: 'OK' });
		});
	});
	app.post('/user/login', (req, res) => {
		user.login(req.body.user, (err, userInfo) => {
			if (err)	handleError(err);
			return res.status(200).json({
				status: 'OK',
				user: userInfo,
				token: jwt.sign(userInfo.toJSON(), config.security.jwtSecret)
			});
		});
	});
}

module.exports = {
	listen: (port) => {
		allRoutes();
		app.listen(port);
	}
};

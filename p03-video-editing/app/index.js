const db = require('./DB/db');
const config = require('./config');
const api = require('./API/index');

db.connect(config.database.connectionString);
api.listen(config.web.port);


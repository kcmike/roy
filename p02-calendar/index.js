var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var express = require('express');
var bcrypt = require('bcrypt-nodejs');

mongoose.connect('mongodb://localhost/calendar');
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error'));
var Schema = mongoose.Schema;

var EventModel = mongoose.model('Event', new Schema({
	title: String,
	description: String,
	startTime: Date,
	endTime: Date,
	location: String
}));

var UserModel = mongoose.model('User', new Schema({
	username: { type: String, unique: true },
	name: String,
	emailAddress: String,
	passwordHash: String
}));

var app = express()

app.use(bodyParser.json());

app.put('/event', (request, response) => {
	const e = request.body.event;
	EventModel.create({
			title: e.title,
			description: e.description,
			startTime: e.startTime,
			endTime: e.endTime,
			location: e.location
		}, (err) => {
			if (err)	handleError(err);
			return response.status(200).json({
				status: 'OK'
			});
	});
});

app.get('/event', (request, response) => {
	EventModel.find().select('title description startTime endTime location').exec((err, events) => {
		if (err)	handleError(err);
		return response.status(200).json({
			status: 'OK',
			events: events
		});
	});
});

app.delete('/event', (request, response) => {
	const eventId = request.body.eventId;
	EventModel.findByIdAndRemove(eventId, (err) => {
		if (err)	handleError(err);
		return response.status(200).json({
			status: 'OK'
		});
	});
});

// this deletes many IDs, and uses recursion to do it (due to everything in nodejs being asynchronous)
app.delete('/events', (request, response) => {
	const eventIds = request.body.eventIds;	// array of ids
	function deleteId(i) {	// i is an index (starting at 0)
		if (i >= eventIds.length) {	// we have finished every id in the array
			return response.status(200).json({
				status: 'OK'
			});
		}
		EventModel.findByIdAndRemove(eventIds[i], (err) => {
			if (err)	handleError;
			return deleteId(i + 1);	// after we have deleted eventsId[i], recurse for the next one
		});
	}
	deleteId(0);
});

app.patch('/event', (request, response) => {
	const eventId = request.body.eventId;
	const updatedData = request.body.event;
	EventModel.findByIdAndUpdate(eventId, updatedData, (err) => {
		if (err)	handleError(err);
		return response.status(200).json({
			status: 'OK'
		});
	});
});

app.post('/searchEvents', (request, response) => {
	var objectToFind = {};
	for (var filter in request.body.filterAll) {
		var currentFilter = request.body.filterAll[filter];
		var fieldName = currentFilter.fieldName;
		if ('exactMatch' in currentFilter) {
			var exactMatch = currentFilter.exactMatch;
			objectToFind[fieldName] = exactMatch;
		} else if ('patternMatch' in currentFilter) {
			var patternMatch = new RegExp(currentFilter.patternMatch);
			objectToFind[fieldName] = patternMatch;
		} else if ('between' in currentFilter) {
			var between = currentFilter.between;
			objectToFind[fieldName] = { $gte: between.low, $lte: between.high };
		} else {
			throw new Error("Invalid search");
		}
	}
	EventModel.find(objectToFind, (err, events) => {
		if (err)	handleError(err);
		return response.status(200).json({
			status: 'OK',
			events: events
		});
	});
});

app.post('/register', (request, response) => {
	var user = request.body.user;
	bcrypt.hash(user.password, null, null, (err, hashedPassword) => {
		if (err)	handleError(err);
		delete user.password;
		user.passwordHash = hashedPassword;
		UserModel.create(user, (err) => {
			if (err)	handleError(err);
			return response.status(200).json({
				status: 'OK'
			});
		});
	});
});

app.post('/login', (request, response) => {
	var user = request.body.user;
	UserModel.findOne({ username: user.username }, (err, dbUser) => {
		if (err)	handleError(err);
		bcrypt.compare(user.password, dbUser.passwordHash, (err, passwordCorrect) => {
			if (err)	handleError(err);
			if (passwordCorrect) {
				return response.status(200).json({
					status: 'OK'
				});
			} else {
				return response.status(403).json({
					status: 'Not OK',
					message: 'Username or password is incorrect'
				});
			}
		});
	});
});

app.listen(8000);
